import std.stdio, std.conv, std.string, std.algorithm, std.array;


version (unittest) {}
 else {
   void main(string[] args)
   {
     auto xyzFile = args[1];
     auto filtrationFile = args[2];
     auto outFile = args[3];

     auto xyz = readXYZ(xyzFile);
     auto filtration = readFiltration(filtrationFile);
     auto indexedFiltration = indexingPoints(filtration, xyz);

     auto fout = File(outFile, "w");
     foreach (entry; indexedFiltration) {
       fout.write(entry.filtrationVal);
       foreach (i; entry.xyzIDs) fout.write("\t", i);
       fout.writeln;
     }
   }
 }


IndexedEntry[] indexingPoints(Entry[] filtration, Point[] xyz)
{
  auto indexedFiltration = new IndexedEntry[](filtration.length);
  
  foreach (i, entry; filtration) {
    ulong[] ids;
    foreach (target; entry.points) {
      foreach (j, p; xyz) {
        if (p.coords == target.coords) {
          ids ~= j;
          break;
        }
      }
    }
    ids = sort(ids).array;
    indexedFiltration[i] = new IndexedEntry(entry.filtrationVal, ids);
  }

  return indexedFiltration;
}


Point[] readXYZ(string filename)
{
  auto app = new Appender!(Point[]);
  
  foreach (line; File(filename).byLine) {
    auto fields = line.to!string.strip.split("\t");
    app.put(new Point(fields.map!(to!double).array));
  }
  
  return app.data;
}



Entry[] readFiltration(string filename)
{
  auto app = new Appender!(Entry[]);
  
  foreach (line; File(filename).byLine) {
    auto fields = line.to!string.strip.split("\t");
    auto dim = fields.length - 2;
    auto filtrationVal = fields[0].to!double;
    auto points = new Point[](dim + 1);

    for (auto i=0; i<dim+1; i++)
      points[i] = new Point(fields[i+1].split(" ")
                            .map!(x => x.to!double).array);

    app.put(new Entry(filtrationVal, points));
  }
  
  return app.data;
}


class IndexedEntry
{
  double filtrationVal;
  ulong[] xyzIDs;
  this(double d, ulong[] p) {
    filtrationVal = d;
    xyzIDs = p;
  }
}


class Entry
{
  double filtrationVal;
  Point[] points;
  this(double d, Point[] p) {
    filtrationVal = d;
    points = p;
  }
}


class Point
{
  double[] coords = null;
  this(double[] c) {coords = c;}
}
