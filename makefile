DMD = dmd
RDMD = rdmd
DFLAGS = -O -release -inline -noboundscheck
TESTDFLAGS = -unittest -main
SRC = $(wildcard *.d)
OBJ = $(SRC:%.d=%.o)
EXE = $(SRC:%.d=%)
COMMAND = $(EXE)


.PHONY: all clean test

all: $(EXE)

check_tie: check_tie.d
	$(DMD) $(DFLAGS) $^

compute_persistent_homology: compute_persistent_homology.d
	$(DMD) $(DFLAGS) $^

compute_persistent_homology_no_optimization: compute_persistent_homology_no_optimization.d
	$(DMD) $(DFLAGS) $^

compute_persistent_homology_no_tree_for_test: compute_persistent_homology_no_tree_for_test.d
	$(DMD) $(DFLAGS) $^

ph_xyz_to_binID: ph_xyz_to_binID.d
	$(DMD) $(DFLAGS) $^

indexing_filtration: indexing_filtration.d
	$(DMD) $(DFLAGS) $^

test:
	$(RDMD) $(TESTDFLAGS) $(SRC)

clean:
	rm *~ $(OBJ) $(EXE)
